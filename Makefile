NAME = calc
CC = clang

CF = -Wall -Werror -Wextra -pedantic
CF += -std=c99

# not supported on gcc/scan-build
CF += -fsanitize=undefined-trap -fsanitize-undefined-trap-on-error

# freebsd
CF += -I/usr/local/include -I./include/

# freebsd
LD = -L/usr/local/lib

CSRC = calc.c parse.c rnp.c shuntingyard.c token.c

OBJS = ${CSRC:%.c=%.o}

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LD)

%.o: src/%.c
	$(CC) -c $< $(CF)

run: $(NAME)
	./$(NAME)

clean:
	-rm $(NAME) a.out *.core *.o *~ .*~

depend:
	$(CC) -E -MM $(CSRC) > .depend


a calculator written in C using shunting-yard and reverse polish notation.

supports:

* +
* -
* \*
* /
* %
* ^ (power)
* ()
* scale=2 (set number of decimals to show, *warning* it rounds)


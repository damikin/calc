#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include "token.h"
#include "parse.h"
#include "shuntingyard.h"
#include "rnp.h"

static char *scale_str = "scale";
static int scale_len = 5;
static int scale = 0;
static char *format = NULL;

void
do_run(char *str) {
	int str_len = strlen(str);
	if(str_len > scale_len && strncmp(str, scale_str, scale_len) == 0) {
		char *p = str + scale_len;
		while(!isdigit(*p) && *p != '\0')
			p++;
		scale = strtoll(p, NULL, 10);
		if(format != NULL) {
			free(format);
			format = NULL;
		}
		return;
	}

	if(format == NULL) {
		asprintf(&format, "%%.%df\n", scale);
	}

	struct token *tokens = parse(str);
	tokens = shunting_yard(tokens);

	double ans;
	if(rnp(tokens, &ans)) {
		/*
 		 * printf's %.#f will ROUND the double/float...
 		 *  let's try to minimize damage?
 		 */
		if(scale > 0)
			printf(format, ans);
		else {
			double intpart;
			modf(ans, &intpart);
			printf(format, intpart);
		}
	}
}

int
main(void) {
	size_t buf_size = 1;
	char *buf = malloc(buf_size);
	if(buf == NULL) {
		fprintf(stderr, "cannot malloc %zu bytes\n", buf_size);
		exit(1);
	}

	char *cur = buf;
	int c;
	for(;;) {
		if(cur >= buf + buf_size) {
			size_t nb_size = buf_size * 2;
			char *nb = malloc(nb_size);
			if(nb == NULL) {
				fprintf(stderr, "cannot malloc %zu bytes\n", nb_size);
				exit(1);
			}

			memcpy(nb, buf, buf_size);
			free(buf);
			buf = nb;
			cur = buf + buf_size;
			buf_size = nb_size;
		}

		c = getchar();
		if(c == '\n' || c == EOF) {
			*cur = '\0';
			if(cur != buf)
				do_run(buf);
			cur = buf;
		} else {
			*cur = c;
			cur++;
		}

		if(c == EOF)
			break;
	}

	free(buf);
	return 0;
}


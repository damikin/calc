NAME = calc
CC = clang
LD = clang

CFLAGS = -Wall -Werror -Wextra -pedantic -std=c99 \
 -I/usr/local/include -I./include/ \
 -fsanitize=undefined-trap -fsanitize-undefined-trap-on-error

# freebsd
LDFLAGS = -L/usr/local/lib -lm

NAMES = calc token parse shuntingyard rnp

SRC = ${NAMES:%=%.c}
OBJ = ${NAMES:%=%.o}

$NAME: $OBJ
	$LD $LDFLAGS -o $NAME $OBJ

token.o: token.h
rnp.o: rnp.h
parse.o: parse.h
shuntingyard.o: shuntingyard.h
%.o: %.c
	$CC $CFLAGS -c $stem.c

run: $NAME
	./$NAME

test:Q: $NAME
	./$NAME <input.test >test.out
	! cmp -s output.test test.out || echo "test failed"

clean:E:
	rm $NAME
	rm *.o
	rm *.core
	rm test.out


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "token.h"
#include "parse.h"

struct token *
make_token(int type) {
	struct token *ret = calloc(1, sizeof(struct token));
	if(ret == NULL)
		return NULL;

	ret->type = type;
	return ret;
}

int
isop(int op) {
	switch(op) {
	case '-':
	case '+':
	case '*':
	case '/':
	case '^':
	case '(':
	case ')':
	case '%':
		return 1;
	default:
		return 0;
	}
}

struct token *
parse(char *cstr) {
	struct token *ret = NULL;
	char *err_str;

	while(*cstr != '\0') {
		struct token *ct = NULL;
		int val = *cstr;

		if(isnumber(val) || val == '.') {
			ct = make_token(NUMBER);
			if(ct == NULL) {
				asprintf(&err_str, "calloc failed: %c", *cstr);
				goto error;
			}

			char *end;
			ct->num = strtod(cstr, &end);
			cstr = end - 1;

			if(ret != NULL && ret->last->op == '-') {
				struct token *temp;
				ret = queue_pop_back(ret, &temp);
				if(ret != NULL && (ret->last->type == NUMBER || ret->last->op == ')'))
					ret = queue_push(ret, temp);
				else {
					free_tokens(temp);
					ct->num *= -1;
				}
			}
		} else if(isop(val)) {
			ct = make_token(OPERATOR);
			if(ct == NULL) {
				asprintf(&err_str, "calloc failed: %c", *cstr);
				goto error;
			}

			ct->op = *cstr;
		}

		if(ct != NULL)
			ret = queue_push(ret, ct);

		cstr++;
	}

	return ret;

error:
	fprintf(stderr, "parse error: %s\n", err_str);
	free(err_str);
	free_tokens(ret);
	return NULL;
}


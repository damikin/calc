#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "token.h"
#include "rnp.h"

int
do_op(struct token *num1, struct token *num2, struct token *op, double *out) {
	if(num1 == NULL || num2 == NULL || op == NULL)
		return 0;

	double n1 = num1->num;
	double n2 = num2->num;
	double ret = 0;

	switch(op->op) {
	case '+': ret = n1 + n2; break;
	case '-': ret = n1 - n2; break;
	case '*': ret = n1 * n2; break;
	case '/': 
		if(n2 == 0)
			return 0;

		ret = n1 / n2;
		break;
	case '%':
		if(n2 == 0)
			return 0;

		ret = fmod(n1, n2);
		break;
	case '^':
		for(ret = 1; n2 > 0; n2--)
			ret *= n1;
		break;
	default:
		return 0;
	}

	if(out != NULL)
		*out = ret;

	return 1;
}

int
rnp(struct token *tokens, double *out) {
	if(tokens == NULL)
		return 0;

	struct token *cur = NULL;
	struct token *stack = NULL;
	char *error_str = NULL;

	while(tokens != NULL) {
		struct token *num1 = NULL;
		struct token *num2 = NULL;
		tokens = queue_pop(tokens, &cur);

		switch(cur->type) {
		case NUMBER:
			stack = stack_push(stack, cur);
			break;
		case OPERATOR:
			stack = stack_pop(stack, &num2);
			stack = stack_pop(stack, &num1);
			if(num1 == NULL || num2 == NULL) {
				asprintf(&error_str, "invalid argument count");
				goto error;
			}

			if(!do_op(num1, num2, cur, &(num1->num))) {
				asprintf(&error_str, "failed to do op: %d %c", cur->type, cur->op);
				goto error;
			}

			stack = stack_push(stack, num1);
			free_tokens(num2);
			break;
		default:
			asprintf(&error_str, "invalid operator: %d %c", cur->type, cur->op);
			goto error;
		}
	}

	if(stack == NULL) {
		asprintf(&error_str, "no numbers on stack");
		goto error;
	}

	if(stack->next != NULL) {
		asprintf(&error_str, "too many numbers on stack");
		goto error;
	}

	if(out != NULL)
		*out = stack->num;

	free_tokens(stack);
	return 1;

error:
	fprintf(stderr, "rnp error: %s\n", error_str);
	free(error_str);
	free_tokens(stack);
	free_tokens(tokens);
	free_tokens(cur);
	return 0;
}


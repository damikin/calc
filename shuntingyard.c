#include <stdio.h>
#include "token.h"
#include "parse.h"
#include "shuntingyard.h"

int get_precedence(char op) {
	switch(op) {
	case '(':
	case ')':
		return 0;

	case '-':
	case '+':
		return 1;

	case '*':
	case '/':
	case '%':
		return 2;

	case '^':
		return 3;
	}
	return -1;
}

struct token *
shunting_yard(struct token *tokens) {
	struct token *output = NULL;
	struct token *stack = NULL;
	struct token *cur = NULL;
	struct token *temp = NULL;

	while(tokens != NULL) {
		tokens = queue_pop(tokens, &cur);

		switch(cur->type) {
		case NUMBER:
			output = queue_push(output, cur);
			break;
		case OPERATOR:
			if(cur->op == ')') {
				while(stack != NULL && stack->op != '(') {
					stack = stack_pop(stack, &temp);
					output = queue_push(output, temp);
				}

				stack = stack_pop(stack, &temp);
				free_tokens(temp);
			} else if(cur->op == '(') {
				stack = stack_push(stack, cur);
			} else {
				for(; stack != NULL;) {
					int c = get_precedence(cur->op);
					int s = get_precedence(stack->op);

					if(!(cur->op == '^' ? c < s : c <= s))
						break;

					stack = stack_pop(stack, &temp);
					output = queue_push(output, temp);
				}

				stack = stack_push(stack, cur);
			}
			break;
		default:
			fprintf(stderr, "ERROR: bad token: %d | %c | %f\n", cur->type, cur->op, cur->num);
			free_tokens(output);
			free_tokens(stack);
			free_tokens(cur);
			return NULL;
		}
	}

	while(stack != NULL) {
		stack = stack_pop(stack, &temp);
		output = queue_push(output, temp);
	}

	return output;
}


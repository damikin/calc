#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "token.h"

void
free_tokens(struct token *tokens) {
	while(tokens != NULL) {
		struct token *cur = tokens;
		tokens = tokens->next;
		free(cur);
	}
}

struct token *
stack_push(struct token *stack, struct token *in) {
	if(in != NULL)
		in->next = stack;
	return in;
}

struct token *
stack_pop(struct token *stack, struct token **out) {
	struct token *p = stack;
	if(out != NULL)
		*out = p;

	if(stack != NULL) {
		stack = stack->next;
		p->next = NULL;
	}

	if(out == NULL)
		free_tokens(p);

	return stack;
}

struct token *
queue_push(struct token *queue, struct token *in) {
	if(in == NULL)
		return queue;

	if(queue == NULL) {
		in->last = in;
		queue = in;
	} else {
		struct token *q = queue->last;
		q->next = in;
		queue->last = in;
	}
	return queue;
}

struct token *
queue_pop(struct token *queue, struct token **out) {
	struct token *p = queue;
	if(out != NULL)
		*out = p;

	if(queue != NULL) {
		queue = queue->next;
		if(queue != NULL)
			queue->last = p->last;

		p->next = NULL;
		p->last = NULL;
	}

	if(out == NULL)
		free_tokens(p);

	return queue;
}

struct token *
queue_pop_back(struct token *queue, struct token **out) {
	if(queue == NULL)
		return NULL;

	struct token *p = queue->last;
	if(out != NULL)
		*out = p;

	if(queue == queue->last) {
		p->last = NULL;
		queue = NULL;
	} else {
		struct token *tp = queue;
		while(tp->next != p)
			tp = tp->next;

		tp->next = NULL;
		queue->last = tp;
	}

	if(out == NULL)
		free_tokens(p);

	return queue;
}


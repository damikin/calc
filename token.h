#ifndef TOKEN_H_
#define TOKEN_H_

enum token_type {
	NONE = 0,
	NUMBER = 1,
	OPERATOR = 2,
};

struct token {
	enum token_type type;
	char op;
	double num;
	struct token *next;
	struct token *last;
};

void free_tokens(struct token *);

struct token *stack_push(struct token *, struct token *);
struct token *stack_pop(struct token *, struct token **);
struct token *queue_push(struct token *, struct token *);
struct token *queue_pop(struct token *, struct token **);
struct token *queue_pop_back(struct token *, struct token **);

#endif

